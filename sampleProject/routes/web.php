<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/hello', function () {
//     return view('hello');
// });


//shortcut way
Route::view('/', 'welcome');
Route::view('/test', 'hello');

//activity 1st part
//Route::view('/url_catalog', 'catalog');
//Route::view('/url_item', 'item');
//Route::view('/url_profile', 'profile');
//Route::view('/url_transactions', 'transactions');


//refactor profiles route to getProfile method in UsersController handle the request
//kung gusto mo dumaan sa controller bago i-return ang view.
//so from -->  Route::view('/url_profile', 'profile');
Route::get('/profile', 'UserController@getProfile');
//notes: when user goes to URL profile, routes will go to UserControllers then
// do the action under function 'getProfile'


//activity 2nd part refactor
Route::get('/transactions', 'UserController@getTransactions');
Route::get('/item', 'ItemController@getItem');
Route::get('/catalog', 'ItemController@getAllItems');
