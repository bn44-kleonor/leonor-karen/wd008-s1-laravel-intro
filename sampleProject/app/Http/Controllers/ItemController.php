<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ItemController extends Controller
{
    //get item
    public function getItem(){
    	return view('item')->with('name', 'item 1');
    }

    //get catalog | get All Items
    public function getAllItems(){
    	return view('catalog')->with('category','category 1');
    }
}