<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    //get user profile
    // public function getProfile(){
    // 	return view('profile');
    // }

	// //refactor to pass a 'SINGLE value' to view
	// public function getProfile()
	// {
	// 	//with method
	// 	return view('profile')->with('name','Karen Leonor');
	// 	// another format below
	// 	//return view('profile',['name'] => 'Karen Leonor']);
	// }

	//refactor to pass a 'MULTIPLE values' to view
	public function getProfile()
	{
		$info = array(
			'name' => 'Karen $info array',
			'age' => '29',
			'courses' => []
		);
		return view('profile')->with($info);
	}

    //get user transactions
    public function getTransactions()
    {
    	$trans = array(
    		'transaction_code' => 'ABCD234',
    		'total' => '300',
    		'items' => ['item1', 'item2', 'item3', 'item4']
    	);
    	return view('transactions')->with($trans);
    }
}